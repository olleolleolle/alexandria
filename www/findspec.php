<?php
require("./connect.php");
require("base.inc.php");

$systems = getall("SELECT id, name FROM sys ORDER BY name");
$genres = getall("SELECT id, name FROM gen WHERE genre = 1 ORDER BY name");
$categories = getall("SELECT id, name FROM gen WHERE genre = 0 ORDER BY name");
$consets = getall("SELECT id, name FROM conset ORDER BY name");
$filelanguages = getall( "SELECT COUNT(DISTINCT data_id) AS count, language FROM files WHERE category = 'sce' AND language != '' AND downloadable = 1 GROUP BY language ORDER BY count DESC", FALSE);

$suggestlanguages = [];
foreach ( $filelanguages AS $filelanguage ) {
    $languagename = getLanguageName( $filelanguage['language'] );
    if ( $languagename != $filelanguage['language'] ) { // only accept known languages
        $suggestlanguages[] = [ 'count' => $filelanguage['count'], 'code' => $filelanguage['language'], 'name' => $languagename ];
    } else {
    }
}

$t->assign('servername',$_SERVER['SERVER_NAME']);
$t->assign('systems',$systems);
$t->assign('genres',$genres);
$t->assign('categories',$categories);
$t->assign('consets',$consets);
$t->assign( 'suggestlanguages', $suggestlanguages );
$t->display('find_advanced.tpl');
?>
